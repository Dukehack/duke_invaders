//game making proccess was started on the 8th of july 2015
//game was finished successfuly on the 24th of july
#include <stdlib.h>
#include <stdio.h>
#include "functions.h"
int main()
{
	allegro_init();

	// output for errors
	std::ofstream error("errors.txt", std::ofstream::out);

	// setup
	install_keyboard(); 
	if(install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0)!=0)
	{
		error << "ERROR install_sound" ; error.close(); return 1;
	}

	//Add all resources needed in the game
	load_resources() ;
	
	// load hiscore
	std::ifstream scores("hiscore.txt", std::ifstream::in);
	scores >> hiscore;
	scores.close();

	// start main program
	init(true);
	do
	{
		idle();

		draw();
		blit(buffer, screen, 0, 0, 0, 0, 800, 600);

	} while(running);

	// destroy bitmaps/sounds
	destroy_bitmap(buffer);
	destroy_bitmap(shipbmp);
	destroy_bitmap(invaderbmp);
	destroy_sample(fire);
	destroy_sample(explode);

	return 0;
}

END_OF_MAIN();