// invader.cpp: implementation of the invader class.
//
//////////////////////////////////////////////////////////////////////

#include "invader.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

invader::invader()
{
	init();
}

invader::~invader()
{

}

void invader::setpos(int _x, int _y)
{
	x = _x;
	y = _y;

	active = true;
}

void invader::draw()
{
	draw_sprite(buffer, invaderbmp, x, y);
}

bool invader::move(int vx)
{
	x+=vx;

	if(active)
	{
		// check for collisions with bullets
		for(int b = 0;b<100;b++)
		{
			if(bul[b].isactive() && bul[b].getdir()<0)
			{
				bool col = true;

				if(bul[b].getx() < x-2) col = false;
				if(bul[b].getx() > x+26) col = false;
				if(bul[b].gety() < y-2) col = false;
				if(bul[b].gety() > y+26) col = false;

				if(col)
				{
					bul[b].destroy();
					kill();
					player1.addscore(100);
					play_sample(explode, 255, 128, 1000, false);
					destroy.newent(x, y, vx);
				}
			}
		}
		// fire bullets
		if(rand()%1000 == 0)
		{
			int free = -1;
			int b;
			for(b = 0;b<100;b++)
			if(!bul[b].isactive())
				free = b;
			if(free != -1)
			{
				bul[free].setpos(x+12, y+26, 20);
				play_sample(fire, 128, 128, 1000, FALSE);
			}
		}
	}

	if(x<0)
	{
		// x = 0;
		return true;
	}
	if(x>776)
	{
		// x = 776;
		return true;
	}
	return false;
}






