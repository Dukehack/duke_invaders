// invader.h: interface for the invader class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INVADER_H__3E3074F6_B2A3_44F4_B629_3F37016A2A4E__INCLUDED_)
#define AFX_INVADER_H__3E3074F6_B2A3_44F4_B629_3F37016A2A4E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "allegro.h"
#include "bullet.h"
#include "player.h"
#include "destroyed.h"
#include <stdlib.h>

// sprites / sounds
extern BITMAP* buffer;
extern BITMAP* invaderbmp;
extern SAMPLE* fire;
extern SAMPLE* explode;

// objects declared in main.cpp
extern bullet bul[100];
extern player player1;
extern destroyed destroy;

class invader  
{
public:
	invader();
	virtual ~invader();

	void init()
	{
		x = 0;
		y = 0;

		active = true;
	}

	void setpos(int, int);

	void draw();
	bool move(int);

	void movedown()
	{
		y+=16;
		if(y > 550 && active)
			player1.loselife();
	}

	bool isactive() { return active; }
	void kill() { active = false; }
private:
	int x;
	int y;
	bool active;
};

#endif // !defined(AFX_INVADER_H__3E3074F6_B2A3_44F4_B629_3F37016A2A4E__INCLUDED_)
