// destroyed.cpp: implementation of the destroyed class.
//
//////////////////////////////////////////////////////////////////////

#include "destroyed.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

destroyed::destroyed()
{
	for(int x = 0;x<50;x++)
		invad[x].active = false;
}

destroyed::~destroyed()
{

}

void destroyed::newent(int _x, int _y, int _vx)
{
	// get free space
	int free = -1;
	for(int x = 0;x<50;x++)
	{
		if(!invad[x].active)
		{
			free = x;
		}
	}
	if(free == -1) return;

	// create ent
	invad[free].active = true;
	invad[free].x = _x;
	invad[free].y = _y;
	invad[free].vx = _vx*2;
	invad[free].vy = -10;
	invad[free].rotate = 0;
}

void destroyed::draw()
{
	for(int x = 0;x<50;x++)
	{
		if(invad[x].active)
		{
			rotate_sprite(buffer, invaderbmp, invad[x].x, invad[x].y, itofix(invad[x].rotate));
			// move x
			invad[x].x+=invad[x].vx;
			// increase velocity
			invad[x].vy+=1;
			// fall
			invad[x].y+=invad[x].vy;
			// rotate
			invad[x].rotate+=invad[x].vx;
			if(invad[x].rotate > 256)
				invad[x].rotate-=256;
			// die
			if(invad[x].y > 600)
				invad[x].active = false;
		}
	}
}






