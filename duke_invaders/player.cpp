// player.cpp: implementation of the player class.
//
//////////////////////////////////////////////////////////////////////

#include "player.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

player::player()
{
	init();
}

player::~player()
{

}

void player::draw()
{
	if(active == 1)
		draw_sprite(buffer, shipbmp, x, 550);
}

int player::move()
{
	// wait after being destroyed
	if(active < 0)
	{
		active--;
		if(active < -100)
		{
			active = 1;
			return 1;
		}
		else
			return 0;
	}

	// check for incoming bullets
	for(int b = 0;b<100;b++)
	{
		if(bul[b].isactive() && bul[b].getdir()>0)
		{
			bool col = true;

			if(bul[b].getx() < x-2) col = false;
			if(bul[b].getx() > x+26) col = false;
			if(bul[b].gety() < 548) col = false;
			if(bul[b].gety() > 574) col = false;

			// hit by bullet
			if(col)
			{
				bul[b].destroy();
				loselife();
			}
		}
	}

	// move
	if(key[KEY_LEFT]) x-=5; 
	if(key[KEY_RIGHT]) x+=5;

	// fire bullets
	if(firedelay > 0)
		firedelay--;
	else
	{
		if(key[KEY_SPACE])
		{
			int free = -1;
			for(int b = 0;b<100;b++)
				if(!bul[b].isactive())
					free = b;
			if(free != -1)
			{
				bul[free].setpos(x+12, 548, -20);
				firedelay = 50;
				play_sample(fire, 128, 128, 1000, FALSE);
			}
		}
	}

	// stop players ship from going off the edge of the screen
	if(x<0) x = 0; //left side position of screen
	if(x>776) x = 776; //Right side position of screen

	return 0;
}










