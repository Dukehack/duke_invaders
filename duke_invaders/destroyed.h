// destroyed.h: interface for the destroyed class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DESTROYED_H__7451A473_91C2_4AA3_8703_35229E908F8C__INCLUDED_)
#define AFX_DESTROYED_H__7451A473_91C2_4AA3_8703_35229E908F8C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "allegro.h"
extern BITMAP* buffer;
extern BITMAP* invaderbmp;

struct ent
{
	int x; // x coor
	int y; // y coor
	int vx; // x vel, rotate direction
	int vy; // y vel
	int rotate; // angle of rotation
	bool active; // active state
};

class destroyed  
{
public:
	destroyed();
	void newent(int, int, int);
	void draw();
	virtual ~destroyed();
protected:
	ent invad[50];
};

#endif // !defined(AFX_DESTROYED_H__7451A473_91C2_4AA3_8703_35229E908F8C__INCLUDED_)
